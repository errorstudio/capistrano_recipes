set :ssl_required, true
before "nginx:generate_config", "nginx:generate_ssl"

namespace :nginx do
	task :generate_ssl, :roles => [:web] do
		if stage == :live
			# exit unless there's a cert and key specified
			unless exists?(:ssl_cert) 
				puts "You need to specify the location of your SSL certificate"
				exit 
			end
			unless exists?(:ssl_key)
				puts "You need to specify the location of your (GPG-encrypted) SSL key"
				exit
			end
			unless exists?(:ip_address)
				puts "You need to specify the IP address for this website"
				exit 
			end
			set :ssl_cert_path, File.join(ssl_dir, ssl_cert)
			set :ssl_key_path, File.join(ssl_dir, ssl_key)
			unless File.exists?(ssl_cert_path) && File.exists?(ssl_key_path)
				puts "You need to put your SSL GPG-encrypted key and cert in the locations you've specified"
				exit
			end
			prompt_for_gpg_phrase
			set :ssl_path, "/etc/nginx/ssl"
			put File.read(ssl_cert_path), "#{ssl_path}/#{primary_domain}.crt"
			key = `echo #{gpg_phrase} | gpg -d -q --passphrase-fd 0 --no-mdc-warning #{ssl_key_path}`
			if $?.success?
				put key, "#{ssl_path}/#{primary_domain}.key"
			else
				puts "Incorrect GPG passphrase"
				exit
			end
		end
	end
end

def prompt_for_gpg_phrase
  unless exists?(:gpg_phrase)
    set(:gpg_phrase) {Capistrano::CLI.ui.ask("GPG passphrase for SSL key: ")}
  end
end