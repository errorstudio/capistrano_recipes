after "nginx:generate_config", "nginx:reload"

namespace :nginx do
  desc "Generate an Nginx config from a template"
  task :generate_config, :roles => [:web] do
    set :nginx_sites_available, "/etc/nginx/sites-available/#{primary_domain}"
    set :nginx_sites_enabled, "/etc/nginx/sites-enabled/#{primary_domain}"

    file = File.join(File.dirname(__FILE__), "templates", "nginx_vhost.conf.erb")
    buffer = ERB.new(File.read(file)).result(binding)
    put buffer, "#{shared_path}/#{primary_domain}.conf"
    send(run_method, "mv #{shared_path}/#{primary_domain}.conf #{nginx_sites_available}")
    send(run_method, "ln -nfs #{nginx_sites_available} #{nginx_sites_enabled}")
  end

  desc "Reload Nginx"
  task :reload, :roles => [:web] do
    sudo "/usr/sbin/invoke-rc.d nginx reload"
  end
end