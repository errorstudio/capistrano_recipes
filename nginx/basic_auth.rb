set :basic_auth_required, true
before "nginx:generate_config", "nginx:add_basic_auth"

namespace :nginx do
	task :add_basic_auth do
	#set up the basic auth if the vars are defined
    if exists?(:basic_auth_required) and basic_auth_required
      set :basic_auth_realm, "Your username and password are required"
      unless exists?(:basic_auth_username) 
      puts "You need to specify the basic auth username"
      exit 
      end
      unless exists?(:basic_auth_password)
        puts "You need to specify the basic auth password"
        exit
      end
      send(run_method, "printf \"#{basic_auth_username}:$(openssl passwd -apr1 #{basic_auth_password})\n\" > #{shared_path}/.htpasswd")
    end
  end
end