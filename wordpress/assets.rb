load(File.join(File.dirname(__FILE__),"..","prompts.rb"))
namespace :wordpress do
	namespace :assets do
		desc "Download assets for the site"
		task :download, :roles => [:web] do
			if ENV.has_key?('delete') and ENV['delete']
				confirm("You're about to download assets from the server and delete your local")
				puts `rsync -rlvz --delete #{web_server}:#{shared_path}/public/wp-content/uploads/ ./public/wp-content/uploads/`
			else		
				puts `rsync -rlvz #{web_server}:#{shared_path}/public/wp-content/uploads/ ./public/wp-content/uploads/`
			end
		end

		desc "Upload assets for the site"
		task :upload, :roles => [:web] do
			if ENV.has_key?('delete') and ENV['delete']
				confirm("You're about to upload assets from the server and delete the remote")
				puts `rsync -rlvz --delete ./public/wp-content/uploads/ #{web_server}:#{shared_path}/public/wp-content/uploads/`
			else		
				puts `rsync -rlvz ./public/wp-content/uploads/ #{web_server}:#{shared_path}/public/wp-content/uploads/`
			end
		end

	end	
end