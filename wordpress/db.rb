load(File.join(File.dirname(__FILE__),"..","prompts.rb"))

namespace :wordpress do
  namespace :db do
    task :create, :roles => [:db] do
      get_db_details
      prompt_for_login
      db_sql = "CREATE DATABASE #{wp_db_name};"
      db_exists = false
      run "mysql --user=#{server_admin_username} --password=#{server_admin_password} --execute=\"show databases;\"" do |channel, stream, data|
        db_exists = db_exists || data.include?(wp_db_name)
      end
      if !db_exists
        puts "Creating database"
        run("mysql --user=#{server_admin_username} --password=#{server_admin_password} --execute=\"#{db_sql}\"")
      else
        puts "#{wp_db_name} exists, so not creating"
      end
    end

    task :grant, :roles => [:db] do
      get_db_details
      prompt_for_login
      puts "Creating user"
      %w{10.% 127.% localhost}.each do |ip|
        user_sql = "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER on #{wp_db_name}.* TO #{wp_db_user}@'#{ip}' IDENTIFIED BY '#{wp_db_password}';"
        run("mysql --user=#{server_admin_username} --password=#{server_admin_password} --execute=\"#{user_sql}\"")
      end
    end

    desc "Upload a local wordpress db to the server"
    task :upload, :roles => [:db] do
      confirm("You're about to overwrite the #{wp_db_name} database on the server")
      get_db_details
      prompt_for_login
      if exists?(:ssl_required) && ssl_required
        db = `mysqldump --host='#{local_db_server}' --port='#{local_db_port}' --user='#{local_db_username}' --password='#{local_db_password}' #{local_db_name} | sed 's/#{local_domain}/#{primary_domain}/g' | sed 's/http:\\/\\/#{primary_domain}/https:\\/\\/#{primary_domain}/g' | gzip`
      else  
        db = `mysqldump --host='#{local_db_server}' --port='#{local_db_port}' --user='#{local_db_username}' --password='#{local_db_password}' #{local_db_name} | sed 's/#{local_domain}/#{primary_domain}/g' | gzip`
      end
      put(db,"/tmp/#{primary_domain}.sql.gz")
      puts "Setting up database"
      run("zcat /tmp/#{primary_domain}.sql.gz | mysql --user=#{server_admin_username} --password=#{server_admin_password} #{wp_db_name}")
      run("rm /tmp/#{primary_domain}.sql.gz")
    end
    
    desc "Download the live db for local development"
    task :download, :roles => [:db] do
      confirm("You're about to overwrite your local database.")
      get_db_details
      prompt_for_login
      if exists?(:ssl_required) && ssl_required
        run "mysqldump --skip-extended-insert --user='#{server_admin_username}' --password='#{server_admin_password}' #{wp_db_name} | sed 's/#{primary_domain}/#{local_domain}/g' | sed 's/https:\\/\\/#{local_domain}/http:\\/\\/#{local_domain}/g' | gzip > /tmp/#{primary_domain}.sql.gz"
      else  
        run "mysqldump --skip-extended-insert --user='#{server_admin_username}' --password='#{server_admin_password}' #{wp_db_name} | sed 's/#{primary_domain}/#{local_domain}/g' | gzip > /tmp/#{primary_domain}.sql.gz"
      end
      get("/tmp/#{primary_domain}.sql.gz","#{primary_domain}.sql.gz")
      `gunzip -c #{primary_domain}.sql.gz | mysql --max_allowed_packet=1000M --host='#{local_db_server}' --port='#{local_db_port}' --user='#{local_db_username}' --password='#{local_db_password}' #{local_db_name}`
      run("rm /tmp/#{primary_domain}.sql.gz")
    end
  end
end

def get_db_details
  if File.exists?(local_wp_config_location)
    set :local_db_username, `grep DB_USER #{local_wp_config_location} | awk -F"', '" '{print $2}' | sed "s/');//g"`.chomp 
    set :local_db_password, `grep DB_PASSWORD #{local_wp_config_location} | awk -F"', '" '{print $2}' | sed "s/');//g"`.chomp
    set :local_db_server_port, `grep DB_HOST #{local_wp_config_location} | awk -F"', '" '{print $2}' | sed "s/');//g"`.chomp
    set :local_db_name, `grep DB_NAME #{local_wp_config_location} | awk -F"', '" '{print $2}' | sed "s/');//g"`.chomp
  else
    set(:local_db_username) {Capistrano::CLI.ui.ask("Local DB Username: ")}
    set(:local_db_password) {Capistrano::CLI.password_prompt("Local DB password: ")}
    set(:local_db_server_port) {Capistrano::CLI.ui.ask("DB location (server:port): ")}
    set(:local_db_name) {Capistrano::CLI.ui.ask("DB Name: ")}
  end
  set :local_db_port, local_db_server_port.split(":").last
  set :local_db_server, local_db_server_port.split(":").first
  set :local_db_server, "127.0.0.1" if local_db_server == "localhost"
end
