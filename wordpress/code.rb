load(File.join(File.dirname(__FILE__),"..","prompts.rb"))
namespace :wordpress do
  namespace :code do
    desc "Download code for the site"
    task :download, :roles => [:web] do
        confirm("You're about to download code from the server")
        puts `rsync -rlvz --exclude='wp-config.php' --exclude='wp-content/uploads' #{web_server}:#{current_path}/public/ ./public/`
    end
  end 
end