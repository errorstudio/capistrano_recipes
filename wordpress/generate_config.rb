namespace :wordpress do

  desc "Generate a config file for wordpress"
  task :generate_config, :roles => [:web] do
  	set :wp_config_template, "wp-config.php.erb"
    require 'open-uri'
    set :secret_keys, open('https://api.wordpress.org/secret-key/1.1/salt/').read
    file = File.join(File.dirname(__FILE__), "templates", wp_config_template) 
    buffer = ERB.new(File.read(file)).result(binding)
    put buffer, "#{shared_path}/public/wp-config.php"
  end

end