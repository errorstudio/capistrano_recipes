set :requires_php, true

load(File.join(File.dirname(__FILE__),"generate_config"))
load(File.join(File.dirname(__FILE__),"db"))
load(File.join(File.dirname(__FILE__),"symlinks"))
load(File.join(File.dirname(__FILE__),"..","nginx","generate_config"))
load(File.join(File.dirname(__FILE__),"..","ownership"))
load(File.join(File.dirname(__FILE__),"assets"))
load(File.join(File.dirname(__FILE__),"code"))
load(File.join(File.dirname(__FILE__),"..","check_changes"))


before "deploy", "deploy:check_changes"
after "deploy:setup", "wordpress:symlinks:setup", "wordpress:generate_config", "nginx:generate_config", "wordpress:db:create", "wordpress:db:grant", "wordpress:db:upload", "deploy:cold"
after "deploy:cold", "wordpress:symlinks:copy_to_shared", "wordpress:symlinks:update", "deploy:set_ownership"
after "deploy", "wordpress:symlinks:update", "deploy:set_ownership"