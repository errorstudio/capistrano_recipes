namespace :rails do
  namespace :db do
    set :db_password, (0...20).map{ [('0'..'9'),('A'..'Z'),('a'..'z')].map {|range| range.to_a}.flatten[rand(64)] }.join
    set(:db_username) {application[0..7] + "_#{stage[0..3]}"}
    set(:db_name) {YAML.load(File.open(File.join(File.dirname(__FILE__), "..","..","database.yml")).read)[stage.to_s]["database"]}
    desc "Create database-[rails_env].yml"
    task :create_config, roles: [:app, :db] do
      server = get_server_name
      set :db_server, server
      file = File.join(File.dirname(__FILE__), "templates", "database.yml.erb")
      buffer = ERB.new(File.read(file)).result(binding)
      put buffer, "#{shared_path}/database-#{rails_env}.yml"
    end

    desc "symlink the config into the right place"
    task :symlink_config do
      set :new_config, File.join(release_path, "config", "database.yml")
      set :shared_config, File.join(shared_path, "database-#{rails_env}.yml")
      run "if [ -f #{new_config} ]; then rm #{new_config}; fi; sudo ln -nfs #{shared_config} #{new_config}"
    end

    task :create, :roles => [:db] do
      prompt_for_login
      db_sql = "CREATE DATABASE #{db_name};"
      db_exists = false
      run "mysql --user=#{server_admin_username} --password=#{server_admin_password} --execute=\"show databases;\"" do |channel, stream, data|
        db_exists = db_exists || data.include?(db_name)
      end
      if !db_exists
        puts "Creating database"
        run("mysql --user=#{server_admin_username} --password=#{server_admin_password} --execute=\"#{db_sql}\"")
      else
        puts "#{db_name} exists, so not creating"
      end
    end

    desc "Grant db rights"
    task :grant, :roles => [:db] do
      prompt_for_login
      puts "Creating user"
      server = get_server_name
      %w{10.% 127.% localhost} + [server].each do |ip|
        user_sql = "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, LOCK TABLES on #{db_name}.* TO '#{db_username}'@'#{ip}' IDENTIFIED BY '#{db_password}';"
        run("mysql --user=#{server_admin_username} --password=#{server_admin_password} --execute=\"#{user_sql}\"")
      end
    end

    desc "Rename database-production.yml to database.yml in config"
    task :rename_database_config, roles: [:db] do
      set :old_config, File.join(release_path, "config", "database-production.yml")

      run "if [ -f #{old_config} ]; then mv #{old_config} #{new_config}; fi"
    end
  end
end

def get_server_name
  server = find_servers(:roles => :db).first
  if exists?(:use_private_mysql_ip) and use_private_mysql_ip
    server = server.to_s.gsub("hosts", "prv")
  end
  server
end

after "deploy:finalize_update", "rails:db:symlink_config"
