namespace :rails do  
  desc "Clear the rails tmp cache"
  task :clear_cache, :roles => :app do
    run "cd #{current_path}; bundle exec rake RAILS_ENV=#{rails_env} tmp:clear"
  end
end