namespace :rails do
  desc "Create application.yml"
  task :setup_figaro_env do
    if File.exists?(File.join(File.dirname(__FILE__), "..", "..", "application.yml"))
      buffer = File.open(File.join(File.dirname(__FILE__), "..", "..", "application.yml")).read
      buffer_path = File.join(release_path, "config", "application.yml")
      put buffer, buffer_path
    end
  end
end
