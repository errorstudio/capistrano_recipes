set :requires_passenger, true

load(File.join(File.dirname(__FILE__),"..","ruby","rvm"))
load(File.join(File.dirname(__FILE__),"symlinks"))
load(File.join(File.dirname(__FILE__),"tmp_cache"))
load(File.join(File.dirname(__FILE__),"assets_ownership"))
load(File.join(File.dirname(__FILE__),"database"))
load(File.join(File.dirname(__FILE__),"figaro"))
load(File.join(File.dirname(__FILE__),"..","nginx","generate_config"))
load(File.join(File.dirname(__FILE__),"..","passenger_server","passenger"))
load(File.join(File.dirname(__FILE__),"..","ownership"))
load(File.join(File.dirname(__FILE__),"..","check_changes"))

after "deploy:setup",'nginx:generate_config'

#database setup
after "deploy:setup", "rails:db:create_config", "rails:db:create", "rails:db:grant", "deploy:set_ownership"
after "deploy:finalize_update", "rails:setup_figaro_env"
after "rails:copy_shared_files", "passenger:generate_init_script", "passenger:restart"
after 'deploy:cold',"rails:copy_shared_files"
after "deploy", "rails:update_shared_symlinks", "deploy:set_ownership","passenger:restart"

after "deploy:setup", "deploy:cold"
after "passenger:restart", "deploy:set_ownership"
