load(File.join(File.dirname(__FILE__),"symlinks.rb"))

namespace :rails do
	desc "Sets the assets to be owned by the running user - necessary to update the mtime"
  task :set_asset_ownership, :roles => [:web] do
    run "sudo chown -R $USER:deployers #{shared_path}/assets"
  end

  desc "Unsets the assets to be owned by the running user - necessary to update the mtime"
  task :unset_asset_ownership, :roles => [:web] do
    run "sudo chown -R #{user}:deployers #{shared_path}/assets"
  end
 end

before "deploy:assets:precompile", "rails:set_asset_ownership"
after "deploy:assets:precompile", "rails:unset_asset_ownership"
