load(File.join(File.dirname(__FILE__),"..","ownership.rb"))

namespace :rails do
  desc "Copy files to the shared folder, ready to be symlinked to"
  task :copy_shared_files do
    shared_files.each do |path|
      run <<-CMD
        if [ -s "#{File.join(release_path, path)}" ];
        then
          cp -R #{File.join(release_path, path)} #{File.join(shared_path)}/;
        fi
      CMD
    end
  end
   
  desc "Update shared symbolic links"
  task :update_shared_symlinks do
    shared_files.each do |path|
      run "rm -rf #{File.join(release_path, path)}"
      run "ln -s #{File.join(deploy_to, "shared", path)} #{File.join(release_path, path)}"
    end
  end

  desc "Created shared and current paths"
  task :create_paths do
    dirs = [deploy_to, shared_path]
    dirs += shared_files.map { |d| File.join(shared_path, d) }
    run "mkdir -p #{dirs.join(' ')}"
  end

end

before "deploy:setup", "rails:create_paths"#, "deploy:set_ownership"
after 'deploy:cold',"rails:copy_shared_files"