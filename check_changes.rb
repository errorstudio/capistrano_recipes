load(File.join(File.dirname(__FILE__),"prompts"))
namespace :deploy do
	task :check_changes do
	    if current_revision == real_revision
	      confirm("You don't have any changes to deploy - have you committed to git?")
	    else
	      # current_revision depends on current_path
	      reset!(:current_path)
	      reset!(:current_revision)
	      reset!(:real_revision)
	    end
	end
end
before "deploy", "deploy:check_changes"