set :bundle_dir, ''
set :bundle_flags, '--system --quiet'
set :bundle_without, [:darwin, :development, :test]
require 'bundler/capistrano'
set :rvm_ruby_string, "ruby-2.0.0-p247@#{application}" unless exists?(:rvm_ruby_string)
set :rvm_type, :system
set :rvm_install_with_sudo, true
set :rvm_install_ruby, :install
require 'rvm/capistrano'

#RVM stuff
before "rvm:install_ruby", "rvm:set_ownership"
after "rvm:install_ruby", "rvm:set_ownership", "rvm:create_gemset"
before "deploy:setup", "rvm:install_ruby", "rvm:remove_hooks"
namespace :rvm do
  desc "Sets the owner to deployment and the group to deployers - specific to Error's setup on the server."
  task :set_ownership do
    run "sudo chown -R deployment:deployers /usr/local/rvm", :shell => :bash
    run "sudo chmod -R 775 /usr/local/rvm", :shell => :bash
  end

  # desc "manually create gemset"
  # task :create_gemset do
  #   run_rvm("gemset create #{application}")
  # end

  desc "remove unnecessary hooks"
  task :remove_hooks do
    #wtf rvm. seriously?
    run "sudo rm -f /usr/local/rvm/hooks/after_use_textmate", :shell => :bash
  end
end