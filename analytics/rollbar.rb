namespace :analytics do
  task :notify_rollbar, :roles => :web do
    unless exists?(:rollbar_token)
      puts "You need to specify your Rollbar token in deploy.rb"
      exit
    end
    set :revision, `git log -n 1 --pretty=format:"%H"`
    set :comment, `git log -n 1 --pretty=format:"%s"`
    set :tag, `git describe #{revision} --tags`
    set :message, "#{tag}: #{comment}"
    set :local_user, `whoami`
    rails_env = fetch(:rails_env, 'production')
    run "curl https://api.rollbar.com/api/1/deploy/ -F access_token=#{rollbar_token} -F environment=#{rails_env} -F revision=#{revision} -F local_username=#{local_user} -F comment='#{message}' >/dev/null 2>&1", :once => true
  end
end

on :exit, "analytics:notify_rollbar"