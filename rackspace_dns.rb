namespace :rackspace do
  task :create_dns_entry, :on_error => :continue do
    set :rackspace_username, "your_user_name"
    set :rackspace_api_key, "your_key"
    dns = Clouddns::Client.new(:username => rackspace_username, :api_key => rackspace_api_key, :location => :uk)
    domain = dns.domains.select {|d| d.name == base_domain}.first
    
    #only create the record if there isn't already one.
    if domain.records.select {|r| r.to_s.match(prelaunch_domain)}.length > 0
      puts "CNAME record exists already"
    else
      puts "Creating CNAME record for #{prelaunch_domain}"
      domain.cname(:name => prelaunch_domain, :data => web_server)
      domain.save
    end
  end
end
