#
# Set your cron_scripts hash in your stage.
# The key should be hourly, daily, weekly, monthly or d (corresponds to the /etc/cron.{hourly,daily,weekly,monthly,d} folders)
#
# Eg:
#
# set :cron_scripts,  {hourly: [File.join(File.expand_path(File.dirname(__FILE__)), "templates", "some_cron_script.sh.erb")]}
#
# The paths in the array value should pass in the full path to the template file

desc "Generate cron scripts"
task :generate_cron_scripts do
  cron_scripts.each do |cron_time, files|
    files.each do |cron_template|
      buffer = ERB.new(File.read(cron_template)).result(binding)

      put buffer, "#{shared_path}/#{cron_time}.#{File.basename(cron_template)}"
      set :cron_filename, File.basename(cron_template).gsub(/\.erb$/, '')
      send(run_method, "sudo mv -f #{shared_path}/#{cron_time}.#{File.basename(cron_template)} /etc/cron.#{cron_time}/#{cron_filename} && sudo chown root:root /etc/cron.#{cron_time}/#{cron_filename} && sudo chmod +x /etc/cron.#{cron_time}/#{cron_filename}")
    end
  end
end
