set :requires_php, true

load(File.join(File.dirname(__FILE__),"generate_config"))
load(File.join(File.dirname(__FILE__),"db"))
load(File.join(File.dirname(__FILE__),"symlinks"))
load(File.join(File.dirname(__FILE__),"..","nginx","generate_config"))
load(File.join(File.dirname(__FILE__),"..","ownership"))

after "deploy:setup", "indexhibit:symlinks:setup", "indexhibit:generate_config", "nginx:generate_config", "indexhibit:db:create", "indexhibit:db:grant", "indexhibit:db:upload", "deploy:cold"
after "deploy:cold", "indexhibit:symlinks:copy_to_shared", "indexhibit:symlinks:update", "deploy:set_ownership"
after "deploy", "indexhibit:symlinks:update", "deploy:set_ownership"