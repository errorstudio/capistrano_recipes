load(File.join(File.dirname(__FILE__),"..","prompts.rb"))


namespace :indexhibit do
  namespace :db do
	set :local_db_server, "localhost"
	set :local_db_port, "8889"
	set :local_db_username, "root"
	set :local_db_password, "root"
     
    task :create, :roles => [:db] do
      prompt_for_login
      db_sql = "CREATE DATABASE #{db_name};"
      db_exists = false
      run "mysql --user=#{server_admin_username} --password=#{server_admin_password} --execute=\"show databases;\"" do |channel, stream, data|
        db_exists = db_exists || data.include?(db_name)
      end
      if !db_exists
        puts "Creating database"
        run("mysql --user=#{server_admin_username} --password=#{server_admin_password} --execute=\"#{db_sql}\"")
      else
        puts "#{db_name} exists, so not creating"
      end
    end

    task :grant, :roles => [:db] do
      prompt_for_login
      user_sql = "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER on #{db_name}.* TO #{db_user}@'10.%' IDENTIFIED BY '#{db_password}';"
      puts "Creating user"
      run("mysql --user=#{server_admin_username} --password=#{server_admin_password} --execute=\"#{user_sql}\"")
    end

    desc "Upload a local wordpress db to the server"
    task :upload, :roles => [:db] do
      confirm("You're about to overwrite the #{db_name} database on the server")
      prompt_for_login
      db = `mysqldump --host='#{local_db_server}' --port='#{local_db_port}' --user='#{local_db_username}' --password='#{local_db_password}' #{local_db_name} | gzip`
      put(db,"/tmp/#{primary_domain}.sql.gz")
      puts "Setting up database"
      run("zcat /tmp/#{primary_domain}.sql.gz | mysql --user=#{server_admin_username} --password=#{server_admin_password} #{db_name}")
      run("rm /tmp/#{primary_domain}.sql.gz")
    end
    
    desc "Download the live db for local development"
    task :download, :roles => [:db] do
      confirm("You're about to overwrite your local database.")
      prompt_for_login
      run "mysqldump --user='#{server_admin_username}' --password='#{server_admin_password}' #{wp_db_name} | gzip > /tmp/#{primary_domain}.sql.gz"
      get("/tmp/#{primary_domain}.sql.gz","#{primary_domain}.sql.gz")
      `gunzip -c #{primary_domain}.sql.gz | mysql --host='#{local_db_server}' --port='#{local_db_port}' --user='#{local_db_username}' --password='#{local_db_password}' #{local_db_name}`
      run("rm /tmp/#{primary_domain}.sql.gz")
    end
  end
end