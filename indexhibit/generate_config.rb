namespace :indexhibit do
	desc "generate remote config for indexhibit"
	task :generate_config do
		file = File.join(File.dirname(__FILE__), "templates", "config.php.erb") 
	    buffer = ERB.new(File.read(file)).result(binding)
	    put buffer, "#{shared_path}/public/config.php"
	end

end
