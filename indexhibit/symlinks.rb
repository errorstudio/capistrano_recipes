namespace :indexhibit do
  namespace :symlinks do

    desc "Setup application symlinks in the public"
    task :setup, :roles => [:web] do
      if app_symlinks
        app_symlinks.each do |link|
          run "mkdir -p #{shared_path}/public/#{link}"
          run "sudo chown -R www-data #{shared_path}/public/#{link}"
        end
      end
    end
    
    desc "Copy files from first deployment to shared location"
    task :copy_to_shared, :roles => [:web] do
      if app_symlinks
        app_symlinks.each {|link| run "sudo cp -R #{current_path}/public/#{link}/* #{shared_path}/public/#{link}" }
      end
    end

    desc "Link public directories to shared location."
    task :update, :roles => [:web] do
      if app_symlinks
        app_symlinks.each do |link|
          run "rm -rf #{current_path}/public/#{link}"
          run "ln -nfs #{shared_path}/public/#{link} #{current_path}/public/#{(link.split("/") - [link.split("/").pop]).join("/")}"
          run "sudo chown -R www-data #{shared_path}/public/#{link}"
        end
      end
      send(run_method, "rm -f #{current_path}/public/ndxz-studio/config/config.php")
      send(run_method, "ln -nfs #{shared_path}/public/config.php #{current_path}/public/ndxz-studio/config/config.php")
    end
  end
end