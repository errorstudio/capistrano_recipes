namespace :passenger do
  # This task with bounce the standalone passenger server.
  # The rails_env and passenger_port are specified in the deploy environment files, ex: "config/deploy/staging.rb"
  desc "Restart Passenger server"
  task :restart, :roles => :web, :except => { :no_release => true } do
    send(run_method, "sudo invoke-rc.d #{application}_#{rails_env}_passenger restart")
  end

  desc "Generate the init script for passenger"
  task :generate_init_script, roles: [:app] do
    # create the shell script that upstart will exec
    file   = File.join(File.dirname(__FILE__), "templates", "passenger_init.erb")
    buffer = ERB.new(File.read(file)).result(binding)
    filename = "#{application}_#{rails_env}_passenger"
    put buffer, "/tmp/#{filename}"
    send(run_method,"sudo mv /tmp/#{filename} /etc/init.d/#{filename}")
    send(run_method, "sudo chmod +x /etc/init.d/#{filename}")
    send(run_method, "sudo update-rc.d #{filename} defaults")
  end

  def wrapper_path
    return "/usr/local/rvm/wrappers/#{rvm_ruby_string}"
  end

  def stop_passenger_command
    return <<-CMD
      if [ -f #{current_path}/tmp/pids/passenger.#{passenger_port}.pid ];
      then
        cd #{current_path} && #{wrapper_path}/bundle exec passenger stop --pid-file #{current_path}/tmp/pids/passenger.#{passenger_port}.pid;
      fi
    CMD
  end

  def start_passenger_command
    return <<-CMD
      rm -f #{current_path}/tmp/pids/passenger.#{passenger_port}.pid;
      cd #{current_path} && set RACK_ENV=#{rails_env} && #{wrapper_path}/bundle exec passenger start -e #{rails_env} -p #{passenger_port} -d;
    CMD
  end

  def restart_passenger_command
    return <<-CMD
      #{stop_passenger_command}
      #{start_passenger_command}
    CMD
  end
end

