set :requires_php, false

load(File.join(File.dirname(__FILE__),"..","nginx","generate_config"))
load(File.join(File.dirname(__FILE__),"..","ownership"))
load(File.join(File.dirname(__FILE__),"..","check_changes"))

after "deploy:setup", "nginx:generate_config", "deploy:cold"
#default dns entry points to web1, so no need to create domain entry if
#it's hosted on that machine.
after "deploy:cold", "deploy:set_ownership"
after "deploy", "deploy:set_ownership"
