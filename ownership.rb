before "deploy:update_code", "deploy:set_ownership"

namespace :deploy do
	  desc "Sets the owner to www-data and the group to deployers"
	  task :set_ownership, :roles => [:web] do
	    run "sudo chown -R www-data:deployers #{deploy_to}"
	    run "sudo chmod -R 775 #{deploy_to}"
	  end
end