def prompt_for_login
  unless exists?(:server_admin_username) && exists?(:server_admin_password)
    set(:server_admin_username) {Capistrano::CLI.ui.ask("Server DB Username: ")}
    set(:server_admin_password) {Capistrano::CLI.password_prompt("Server DB password: ")}
  end
end

def confirm(message)
  Capistrano::CLI.ui.say(message)

  agree = Capistrano::CLI.ui.agree("Continue (Y, [N])? ") do |q|
    q.default = 'n'
  end

  exit unless agree
end