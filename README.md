# Error Studio's Capistrano Recipes

## Introduction

This is a selection of includable [Capistrano](http://capistranorb.com/)recipes which make life easy for deployment. __Note that these recipes are for Capistrano 2__. We use version 2.15.5.

## Licence
[MIT](http://opensource.org/licenses/MIT) - but if you could contribute back to this repository with pull requests that would be awesome!