set :requires_php, false
set :requires_passenger, false

load(File.join(File.dirname(__FILE__),"..","nginx","generate_config"))
load(File.join(File.dirname(__FILE__),"..","ruby","rvm"))
load(File.join(File.dirname(__FILE__),"..","ownership"))
load(File.join(File.dirname(__FILE__),"..","check_changes"))
load(File.join(File.dirname(__FILE__),"build"))

after "deploy:setup", "nginx:generate_config", "deploy:cold"
after "deploy:cold", "deploy:set_ownership"
after "deploy", "deploy:set_ownership", "jekyll:build"