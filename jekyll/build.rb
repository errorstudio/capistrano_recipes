namespace :jekyll do
  desc "Build Jekyll"
  task :build do
    run "cd #{deploy_to}/current && bundle exec jekyll build"
  end
end

after "jekyll:build", "deploy:set_ownership"